package com.example.sd.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {
    @GetMapping("/")
    public String getIndex() {
        return "index";
    }

    @GetMapping("/admin")
    public String getAdmin() {
        return "admins";
    }

    @GetMapping("/user")
    public String getUser() {
        return "users";
    }

    @GetMapping("/logoff")
    public String logOff(HttpServletRequest request) throws ServletException {
        request.logout();
        return "redirect:/";
    }
}
